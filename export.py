from fractions import Fraction
from pathlib import Path

import xlsxwriter
from xlsxwriter.worksheet import Worksheet

from getters import (
    RightInfoGetterManager,
    CadastralNumberGetter,
    INNGetter,
    NameHolderRightGetter,
    RightTypeGetter,
    RightNumberGetter,
    RightDateGetter,
    RightShareGetter,
    OwnershipTypeGetter,
    EncumbranceTypeGetter,
    EncumbranceTypeTextGetter,
    EncumbranceOwnerInnGetter,
    EncumbranceOwnerTitleGetter,
    EncumbranceRegistrationNumberGetter,
    EncumbranceRegistrationDateGetter,
    EncumbranceRegistrationReasonGetter,
    EncumbranceDurationGetter,
    RightHistoryEndDateGetter,
    RightHistoryEndReasonGetter,
)
from loader import RightsLoader, HistoryLoader
from models import RightParam, RightHistoryParam


class XlsxExport:
    def __init__(self, file_path: Path | str):
        if isinstance(file_path, str):
            file_path = Path(file_path)

        if file_path.as_posix().endswith('/'):
            raise RuntimeError(
                f'XlsxExport.__init__ expect  argument "file_path" - file path',
            )

        suffixes = file_path.suffixes
        if not (suffixes and suffixes[-1] == '.xlsx'):
            file_path = file_path.with_suffix('.xlsx')

        self.file_path = Path.cwd() / 'export_file/' / file_path
        self.workbook = xlsxwriter.Workbook(
            self.file_path,
            {'constant_memory': True},
        )

        self.batch_size = 50
        self.fraction_format = self.workbook.add_format({'num_format': '#""?/?'})

        self.rights_loader = RightsLoader()
        self.history_rights_loader = HistoryLoader()
        self.getter = RightInfoGetterManager(
            [
                CadastralNumberGetter(),
                INNGetter(),
                NameHolderRightGetter(),
                RightTypeGetter(),
                RightNumberGetter(),
                RightDateGetter(),
                RightShareGetter(),
                OwnershipTypeGetter(),
                EncumbranceTypeGetter(),
                EncumbranceTypeTextGetter(),
                EncumbranceOwnerInnGetter(),
                EncumbranceOwnerTitleGetter(),
                EncumbranceRegistrationNumberGetter(),
                EncumbranceRegistrationDateGetter(),
                EncumbranceRegistrationReasonGetter(),
                EncumbranceDurationGetter(),
                RightHistoryEndDateGetter(),
                RightHistoryEndReasonGetter(),
            ],
        )

    def _write_column_names(
            self,
            names: list[str],
            worksheet:  Worksheet,
    ) -> None:
        for col, name in enumerate(names):
            worksheet.write(0, col, name)

    def _get_column_right_getter_map(self) -> dict[str, str]:
        columns = RightParam.select(
            RightParam.title,
            RightParam.slug,
        ).where(
            RightParam.use_for_export == True,
        ).order_by(+RightParam.id).tuples().execute()

        fields_map_for_export = {'Кадастровый номер': 'cadastral_number'}
        fields_map_for_export |= {
            column_name: slug for column_name, slug in columns
        }

        return fields_map_for_export

    def _get_column_right_history_getter_map(self) -> dict[str, str]:
        columns = RightHistoryParam.select(
            RightHistoryParam.title,
            RightHistoryParam.slug,
        ).where(
            RightHistoryParam.use_for_export == True,
        ).order_by(+RightHistoryParam.id).tuples().execute()

        fields_map_for_export = {'Кадастровый номер': 'cadastral_number'}
        fields_map_for_export |= {
            column_name: slug for column_name, slug in columns
        }

        return fields_map_for_export

    def _write(self, worksheet, loader, right_worksheet_fields_map):
        column_names = right_worksheet_fields_map.keys()
        self._write_column_names(column_names, worksheet)

        row = 1
        for rights in loader.get_batch(self.batch_size):
            for right in rights:
                for col, column_name in enumerate(column_names):
                    slug = right_worksheet_fields_map[column_name]
                    try:
                        value = self.getter.get_data_by_slug(slug, right)
                    except Exception:
                        value = 'N/N'

                    if isinstance(value, Fraction):
                        format = self.fraction_format
                    else:
                        format = None

                    worksheet.write(row, col, value, format)

                row += 1

    def upload_rights_and_encumbrance(self):
        right_worksheet = self.workbook.add_worksheet(
            'Действующие права и ограничения',
        )
        right_worksheet_fields_map = self._get_column_right_getter_map()
        self._write(
            right_worksheet,
            self.rights_loader,
            right_worksheet_fields_map,
        )

        history_worksheet = self.workbook.add_worksheet(
            'История перехода прав',
        )
        history_worksheet_fields_map = self._get_column_right_history_getter_map()
        self._write(
            history_worksheet,
            self.history_rights_loader,
            history_worksheet_fields_map,
        )

        self.workbook.close()


if __name__ == '__main__':
    import logging

    logger = logging.getLogger('peewee')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

    XlsxExport('my.excel.test').upload_rights_and_encumbrance()
