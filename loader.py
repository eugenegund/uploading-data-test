from typing import Iterable

from peewee import JOIN

from models import (
    Right,
    Owner,
    Encumbrance,
    RealEstate,
    Registration,
    RightType,
    OwnershipForm,
    EncumbranceType,
    RightHistory,
)


class RightsLoader:
    def get_batch(self, count: int) -> Iterable[list[Right]]:
        count_rights = Right.select().count()

        for start_batch in range(0, count_rights + 1, count):
            rights = Right.select(
                Right,
                Owner,
                RealEstate.cadastral_number,
                Registration,
                RightType,
                Encumbrance,
                EncumbranceType,
                OwnershipForm,
            ).join_from(
                Right,
                Owner,
                JOIN.LEFT_OUTER,
            ).join_from(
                Right,
                RealEstate,
                JOIN.LEFT_OUTER,
            ).join_from(
                Right,
                Registration,
                JOIN.LEFT_OUTER,
            ).join_from(
                Registration,
                RightType,
                JOIN.LEFT_OUTER,
            ).join_from(
                Right,
                Encumbrance,
                JOIN.LEFT_OUTER,
            ).join_from(
                Encumbrance,
                EncumbranceType,
                JOIN.LEFT_OUTER,
            ).join_from(
                Right,
                OwnershipForm,
                JOIN.LEFT_OUTER,
            ).offset(start_batch).limit(count)
            yield rights


class HistoryLoader:
    def get_batch(self, count: int) -> Iterable[list[RightHistory]]:
        count_rights = RightHistory.select().count()

        for start_batch in range(0, count_rights + 1, count):
            history_rights = RightHistory.select(
                RightHistory,
                Owner,
                RealEstate.cadastral_number,
                Registration,
            ).join_from(
                RightHistory,
                Owner,
                JOIN.LEFT_OUTER,
            ).join_from(
                RightHistory,
                RealEstate,
                JOIN.LEFT_OUTER,
            ).join_from(
                RightHistory,
                Registration,
                JOIN.LEFT_OUTER,
            ).join_from(
                Registration,
                RightType,
                JOIN.LEFT_OUTER,
            ).offset(start_batch).limit(count)
            yield history_rights
