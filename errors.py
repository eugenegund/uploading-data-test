class GetterError(Exception):
    pass


class GetterNotFound(GetterError):
    def __init__(self, slug: str):
        self.slug = slug

    def __str__(self):
        return f'Getter with slug "{self.slug}" not registered. ' \
               f'Please check your GetterManager.'
