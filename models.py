from peewee import (
    Model,
    PostgresqlDatabase,
    CharField,
    ForeignKeyField,
    IntegerField,
    DateTimeField,
    BooleanField,
    TextField,
)


from playhouse.postgres_ext import DateTimeTZField

db = PostgresqlDatabase(
    '',
    host='',
    port=5432,
    user='',
    password='',
)


class RightParam(Model):
    id = IntegerField(primary_key=True)
    title = CharField(max_length=200, null=False)
    slug = CharField(max_length=200, null=False)
    use_for_export = BooleanField(null=False)

    class Meta:
        table_name = 'right_holders_rightparam'
        database = db


class RightHistoryParam(Model):
    id = IntegerField(primary_key=True)
    title = TextField(null=False)
    slug = CharField(max_length=100, null=False)
    use_for_export = BooleanField(null=False)

    class Meta:
        table_name = 'right_holders_rightshistoryparam'
        database = db


class EncumbranceType(Model):
    id = IntegerField(primary_key=True)
    code = CharField(max_length=15, null=False)
    title = TextField(null=False)

    class Meta:
        table_name = 'right_holders_encumbrancetype'
        database = db


class Encumbrance(Model):
    id = IntegerField(primary_key=True)
    registration_date = DateTimeField(null=True)
    registration_number = CharField(max_length=45, null=True)
    duration_start = DateTimeField(null=True)
    duration_term = TextField(null=True)
    duration_end = DateTimeField(null=True)
    created = DateTimeTZField(null=False)
    type = ForeignKeyField(EncumbranceType, lazy_load=False, null=True)
    registration_reason = TextField(null=True)

    class Meta:
        table_name = 'right_holders_encumbrance'
        database = db


class RightSubject(Model):
    id = IntegerField(primary_key=True)
    code = CharField(max_length=15, null=False)
    title = CharField(max_length=200, null=False)

    class Meta:
        table_name = 'right_holders_rightsubject'
        database = db


class OwnerType(Model):
    id = IntegerField(primary_key=True)
    title = CharField(max_length=50, null=True)
    rosreestr_slug = CharField(max_length=50, null=True)

    class Meta:
        table_name = 'right_holders_ownertype'
        database = db


class Owner(Model):
    id = IntegerField(primary_key=True)
    title = TextField(null=True)
    inn = CharField(max_length=12, null=True)
    created = DateTimeTZField(null=True)
    right_subject = ForeignKeyField(RightSubject, lazy_load=True, null=False)
    type = ForeignKeyField(OwnerType, lazy_load=True, null=False)

    class Meta:
        table_name = 'right_holders_owner'
        database = db


class EncumbranceOwners(Model):
    id = IntegerField(primary_key=True)
    encumbrance = ForeignKeyField(
        Encumbrance,
        backref='encumbrance_owners',
        lazy_load=False,
        null=False,
    )
    owner = ForeignKeyField(
        Owner,
        backref='encumbrance_owners',
        lazy_load=False,
        null=False,
    )

    class Meta:
        table_name = 'right_holders_encumbrance_owners'
        database = db


class RightType(Model):
    id = IntegerField(primary_key=True)
    code = CharField(max_length=15, null=False)
    title = TextField(null=False)

    class Meta:
        table_name = 'right_holders_righttype'
        database = db


class Registration(Model):
    id = IntegerField(primary_key=True)
    registration_number = CharField(max_length=45, null=False)
    registration_date = DateTimeField(null=True)
    end_date = DateTimeField(null=True)
    created = DateTimeTZField(null=False)
    type = ForeignKeyField(RightType, lazy_load=True, null=False)
    name = TextField(null=True)

    class Meta:
        table_name = 'right_holders_registration'
        database = db


class OwnershipForm(Model):
    id = IntegerField(primary_key=True)
    code = TextField(null=False)
    title = TextField(null=False)

    class Meta:
        table_name = 'right_holders_ownershipform'
        database = db


class RealEstate(Model):
    id = IntegerField(primary_key=True)
    cadastral_number = CharField(max_length=255, null=False)

    class Meta:
        table_name = 'real_estates_realestate'
        database = db


class Right(Model):
    id = IntegerField(primary_key=True)
    data_origin_id = IntegerField(null=False)
    encumbrance = ForeignKeyField(Encumbrance, lazy_load=False, null=True)
    owner = ForeignKeyField(Owner, lazy_load=False, null=True)
    real_estate = ForeignKeyField(RealEstate, lazy_load=False, null=False)
    registration = ForeignKeyField(Registration, lazy_load=False, null=True)
    original_right_id = IntegerField(null=True)
    share_denumerator = IntegerField(null=True)
    share_numerator = IntegerField(null=True)
    deleted = BooleanField(null=False)
    user_id = IntegerField(null=True)
    is_deleted_by_indexation = BooleanField(null=False)
    ownership_form = ForeignKeyField(
        OwnershipForm,
        lazy_load=False,
        null=True,
    )

    class Meta:
        table_name = 'right_holders_right'
        database = db


class RightHistory(Model):
    id = IntegerField(primary_key=True)
    created = DateTimeTZField(null=False)
    extract_id = IntegerField(null=False)
    owner = ForeignKeyField(Owner, lazy_load=False, null=True)
    real_estate = ForeignKeyField(RealEstate, lazy_load=False, null=False)
    registration = ForeignKeyField(Registration, lazy_load=False, null=True)
    end_reason = TextField(null=True)
    share_denumerator = IntegerField(null=True)
    share_numerator = IntegerField(null=True)

    class Meta:
        table_name = 'right_holders_rightshistory'
        database = db
