from datetime import datetime
from fractions import Fraction
from typing import Any, Callable

from errors import GetterNotFound, GetterError
from models import Right, Owner, EncumbranceOwners, RightHistory


class Getter:
    @property
    def slug(self) -> str:
        raise NotImplementedError

    def run(self, right: Right | RightHistory) -> Any:
        raise NotImplementedError


class INNGetter(Getter):
    slug = 'owner_inn'

    def run(self, right: Right | RightHistory) -> str:
        if right.owner:
            return right.owner.inn

        return ''


class CadastralNumberGetter(Getter):
    slug = 'cadastral_number'

    def run(self, right: Right | RightHistory) -> str:
        return right.real_estate.cadastral_number


class NameHolderRightGetter(Getter):
    slug = 'owner_title'

    def run(self, right: Right | RightHistory) -> str:
        if right.owner and right.owner.title:
            return right.owner.title

        return ''


class RightTypeGetter(Getter):
    slug = 'right_type'

    def run(self, right: Right | RightHistory) -> str:
        if right.registration:
            return right.registration.type.title

        return ''


class RightNumberGetter(Getter):
    slug = 'registration_number'

    def run(self, right: Right | RightHistory) -> str:
        if right.registration:
            return right.registration.registration_number

        return ''


class RightDateGetter(Getter):
    slug = 'registration_date'

    def run(self, right: Right | RightHistory) -> str | None:
        date = None
        if right.registration:
            date: None | datetime = right.registration.registration_date

        if date is None:
            return ''

        return date.strftime('%d\%m\%Y')


class RightHistoryEndDateGetter(Getter):
    slug = 'end_date'

    def run(self, right: RightHistory) -> str | None:
        date = None
        if right.registration:
            date: None | datetime = right.registration.end_date

        if date is None:
            return ''

        return date.strftime('%d\%m\%Y')


class RightShareGetter(Getter):
    slug = 'shares'

    def run(self, right: Right | RightHistory) -> Fraction | str:
        if right.share_numerator and right.share_denumerator:
            return Fraction(right.share_numerator, right.share_denumerator)
        return ''


class OwnershipTypeGetter(Getter):
    slug = 'type'

    def run(self, right: Right | RightHistory) -> str:
        if right.ownership_form:
            return right.ownership_form.title

        return ''


class EncumbranceTypeGetter(Getter):
    slug = 'encumbrance_type'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance and right.encumbrance.type:
            return right.encumbrance.type.code

        return ''


class EncumbranceTypeTextGetter(Getter):
    slug = 'encumbrance_type_by_text'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance and right.encumbrance.type:
            return right.encumbrance.type.title

        return ''


class EncumbranceOwnerInnGetter(Getter):
    slug = 'encumbrance_owner_inn'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance:
            # N + 1
            en_owner = EncumbranceOwners.select(EncumbranceOwners, Owner).where(
                EncumbranceOwners.encumbrance == right.encumbrance,
            ).join_from(EncumbranceOwners, Owner).get_or_none()
            if en_owner is not None and en_owner.owner:
                return en_owner.owner.inn or ''

        return ''


class EncumbranceOwnerTitleGetter(Getter):
    slug = 'encumbrance_owner_title'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance:
            # N + 1
            en_owner = EncumbranceOwners.select(EncumbranceOwners, Owner).where(
                EncumbranceOwners.encumbrance == right.encumbrance,
            ).join_from(EncumbranceOwners, Owner).get_or_none()
            if en_owner is not None and en_owner.owner:
                return en_owner.owner.title or ''

        return ''


class EncumbranceRegistrationNumberGetter(Getter):
    slug = 'encumbrance_registration_number'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance and right.encumbrance.registration_number:
            return right.encumbrance.registration_number

        return ''


class EncumbranceRegistrationDateGetter(Getter):
    slug = 'encumbrance_registration_date'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance and right.encumbrance.registration_date:
            return right.encumbrance.registration_date.strftime('%d\%m\%Y')

        return ''


class EncumbranceRegistrationReasonGetter(Getter):
    slug = 'encumbrance_registration_reason'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance and right.encumbrance.registration_reason:
            return right.encumbrance.registration_reason

        return ''


class EncumbranceDurationGetter(Getter):
    slug = 'encumbrance_duration'

    def run(self, right: Right | RightHistory) -> str:
        if right.encumbrance and right.encumbrance.duration_term:
            return right.encumbrance.duration_term

        return ''


class RightHistoryEndReasonGetter(Getter):
    slug = 'end_reason'

    def run(self, right: RightHistory) -> str:
        if right.end_reason:
            return right.end_reason

        return ''


class RightInfoGetterManager:
    def __init__(self, actions: list[Getter]) -> None:
        self.action_map: dict[str, Callable] = {}
        for action in actions:
            self.action_map[action.slug] = action.run

    def get_data_by_slug(self, slug: str, right: Right | RightHistory) -> Any:
        if slug not in self.action_map:
            raise GetterNotFound(slug)

        try:
            return self.action_map[slug](right=right)
        except Exception as e:
            raise GetterError() from e
